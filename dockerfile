# 构建阶段
FROM node:16.13-buster as builder

# 设置工作路径为/app
WORKDIR /app

RUN npm install cnpm -g --registry=https://registry.npm.taobao.org

# 先拷贝package只要package不改变 部署的时候就不需要重新下载依赖
COPY package.json package.json

RUN cnpm i
# 拷贝dockerfile路径下面的所有文件到镜像的/app/目录下面
COPY . .

RUN cnpm run build

# 运行镜像
FROM nginx

RUN rm /etc/nginx/conf.d/default.conf

# 从构建镜像中拷贝。这里的public 修改为打包之后生成的文件夹位置 如果为dist 就是/app/dist
COPY --from=builder /app/dist/ /usr/share/nginx/html/

COPY --from=builder /app/default.conf /etc/nginx/conf.d/
