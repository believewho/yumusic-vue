import Vue from 'vue'

import {
    Form,
    FormItem,
    Input,
    Cascader,
    Button,
    Upload,
    Card,
    Dropdown,
    DropdownItem,
    DropdownMenu,
    Tabs,
    TabPane,
    Badge,
    Dialog,
    DatePicker
} from "element-ui"


Vue.use(DatePicker)
Vue.use(Dialog)
Vue.use(TabPane)
Vue.use(Tabs)
Vue.use(Badge)
Vue.use(Input)
Vue.use(Card)
Vue.use(Form)
Vue.use(Cascader)
Vue.use(FormItem)
Vue.use(Button)
Vue.use(Upload)
Vue.use(Dropdown)
Vue.use(DropdownItem)
Vue.use(DropdownMenu)