import * as types from './mutation-types'

const mutations = {
  // 修改audio元素
  [types.SET_AUDIOELE](state, audioEle) {
    state.audioEle = audioEle
  },
  // 修改播放模式
  [types.SET_PLAYMODE](state, mode) {
    state.mode = mode
  },
  // 修改播放状态
  [types.SET_PLAYING](state, playing) {
    state.playing = playing
  },
  // 修改播放列表
  [types.SET_PLAYLIST](state, playlist) {
    state.playlist = playlist
  },
  // 修改顺序列表
  [types.SET_ORDERLIST](state, orderList) {
    state.orderList = orderList
  },
  // 修改当前音乐索引
  [types.SET_CURRENTINDEX](state, currentIndex) {
    state.currentIndex = currentIndex
  },
  // 修改播放历史列表
  [types.SET_HISTORYLIST](state, historyList) {
    state.historyList = historyList
  },
  // 修改用户
  [types.SET_USER](state, user) {
    state.user = user
  },
  // 修改音乐进度条的百分比
  [types.SET_MUSICPERCENT](state, percent) {
    state.musicPercent = percent
  },
  // 设置歌词列表
  [types.SET_LYRICSLIST](state, list) {
    state.lyricsList = list
  },
  [types.SET_CURRENTLYRICSINDEX](state, index) {
    state.currentLyricsIndex = index
  },

  // 设置token
  [types.SET_TOKEN](state, token) {
    state.token = token
  },
  // 设置keywords
  [types.SET_KEYWORDS](state, keywords) {
    state.keywords = keywords
  },
  // 设置类型
  [types.SET_SEARCHTYPE](state, type) {
    state.searchEntityType = type
  },
  // 设置其他用户
  [types.SET_ANOTHERUSER](state, user) {
    state.anotherUser = user 
  }
}

export default mutations
