
// audio元素
export const audioEle = state => state.audioEle
// 播放模式
export const mode = state => state.mode
// 播放状态
export const playing = state => state.playing
// 播放列表
export const playlist = state => state.playlist
// 顺序列表
export const orderList = state => state.orderList
// 当前音乐索引
export const currentIndex = state => state.currentIndex
// 当前音乐
export const currentMusic = state => {
  return state.playlist[state.currentIndex] || {}
}
// 播放历史列表
export const historyList = state => state.historyList
// 用户
export const user = state => state.user
// 进度条百分比
export const musicPercent = state => state.musicPercent
// 歌词
export const lyricsList = state => state.lyricsList
// 歌词下标
export const currentLyricsIndex = state => state.currentLyricsIndex
// 用户token
export const token = state => state.token

// 用户搜索关键词
export const keywords = state => state.keywords
// 用户搜索词语
export const searchType = state => state.searchEntityType
// 当前访问的主页是否为用户自己
export const isSelf = state => {
  if (state.anotherUser === null || state.anotherUser === "") {
    return true
  }
  return state.anotherUser.id === state.user.id
}
// 另外的用户信息
export const anotherUser = state => state.anotherUser