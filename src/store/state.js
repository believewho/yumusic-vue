import { playMode } from '@/config'
import { getHistoryList, getMode } from '@/utils/storage'

const state = {
  audioEle: null, // audio元素
  mode: Number(getMode()) || playMode.listLoop, // 播放模式，默认列表循环
  playing: false, // 播放状态
  playlist: [], // 播放列表
  orderList: [], // 顺序列表
  currentIndex: 0, // 当前音乐索引
  musicPercent: 0, // 当前音乐播放百分比
  historyList: getHistoryList() || [], // 播放历史列表
  user: null, // 用户user
  lyricsList: [], // 歌词列表
  currentLyricsIndex: -1, // 当前的歌词下标
  token: '', // 用户的token 没有则说明没有登录
  keywords: '', // 用户搜索的关键词
  searchEntityType: '', // 搜索的实体的类型
  anotherUser: '' // 其他用户
}

export default state
