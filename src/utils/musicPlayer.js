import { playMode } from '@/config'
import { setHistoryList } from './storage'
import store from '@/store'
import { HttpManager } from '../api'
// 重试次数
// let retry = 1



export function initAudio(that) {
    const ele = that.audioEle
    ele.src = that.currentMusic.url

    // 获取当前播放时间
    ele.ontimeupdate = () => {
        that.currentTime = ele.currentTime
    }
    // 音频缓冲事件
    // ele.onprogress = () => {
    //     try {
    //         if (ele.buffered.length > 0) {
    //             const duration = that.currentMusic.duration
    //             let buffered = 0
    //             ele.buffered.end(0)
    //             buffered =
    //                 ele.buffered.end(0) > duration ? duration : ele.buffered.end(0)
    //             that.currentProgress = buffered / duration
    //         }
    //     } catch (e) {
    //         console.log(e)
    //     }
    // }
    ele.oncanplay = () => {
        console.log("canplay")
    }
    // 开始播放音乐
    ele.onplay = () => {
        let timer
        clearTimeout(timer)
        timer = setTimeout(() => {
            that.musicReady = true
        }, 100)
        setHistoryList(that.currentMusic)
    }
    // 当前音乐播放完毕
    ele.onended = () => {
        if (that.mode === playMode.loop) {
            that.changeSong(0)
        } else {
            that.changeSong(1)
        }

    }
    ele.oncanplay = () => {
        if (that.historyList) {
            if (
                that.historyList.length === 0 ||
                that.currentMusic.id !== that.historyList[0].id
            ) {
                that.setHistory(that.currentMusic)
            }
        }
        HttpManager.playcountIncrease({
            songId: store.getters.currentMusic.id
        })

    }
    // 当音频已暂停时
    ele.onpause = () => {
        that.setPlaying(false)
    }
    // 音乐播放出错
    ele.onerror = () => {
        let retry = 1
        if (that.currentIndex === -1) {
            return
        }
        if (retry === 0) {
           return 
            // that.next(true)
        } else {
            retry -= 1
            ele.url = that.currentMusic.url
            ele.load()
        }
        // console.log('播放出错啦！')
    }
}


