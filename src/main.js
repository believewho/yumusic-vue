import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import $ from 'jquery'
import 'animate.css'
import vuescroll from 'vuescroll'
import '@/element-ui'

import {
  Tag, 
  Avatar,
  Divider, 
  Tooltip, 
  Slider, 
  Pagination,
  Icon,
  Empty,
  Modal,
  message,
  Switch,
  Button
} from 'ant-design-vue';

// axios.defaults.baseURL = 'http://localhost:9000/api'


Vue.prototype.$http = axios
Vue.prototype.$ = $
Vue.prototype.$staticSourcePrefix = 'http://localhost'

Vue.config.productionTip = false
Vue.use(vuescroll)
Vue.component(Avatar.name, Avatar)
Vue.component(Tag.name, Tag)
Vue.component(Divider.name, Divider)
Vue.component(Tooltip.name, Tooltip)
Vue.component(Switch.name, Switch)
Vue.component(Slider.name, Slider)
Vue.component(Pagination.name, Pagination)
Vue.component(Button.name, Button)
const IconFont = Icon.createFromIconfontCN({
  scriptUrl: '//at.alicdn.com/t/font_3183240_jezkbbyw3ue.js'
})
Vue.component(Icon.name, Icon)
Vue.component(Empty.name, Empty)
Vue.use(Modal)
Vue.component('IconFont', IconFont)
Vue.use(window['vue-cropper'].default)

Vue.prototype.$message = message

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
