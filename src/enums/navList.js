export const spaceList = [
    {
        value: '我喜欢',
        path: '/myspace/like',
        chlidren: [
            { value: '歌曲', path: '/myspace/like/song' },
            { value: '歌单', path: '/myspace/like/playlist' },
            { value: '专辑', path: '/myspace/like/album' }
        ]
    },
    {
        value: '我创建的歌单',
        path: '/myspace/playlist'
    },
    {
        value: '关注', 
        path: '/myspace/focus',
        chlidren: [
            { value: '歌手', path: '/myspace/focus/artist' },
            { value: '用户', path: '/myspace/focus/users' }
        ]
    },
    { 
        value: '粉丝', 
        path: '/myspace/fans' 
    },
    {
        value: '歌曲管理',
        path: '/myspace/song'
    },
    {
        value: '每日推荐',
        path: '/myspace/daily'
    },
    {
        value: '历史记录',
        path: '/myspace/history'
    }
]

export const Toplist = [
    {
        title: '巅峰榜',
        lists: [
            {name:'飙升榜', value: {time: 7}},
            {name:'热度榜', value: {}}
        ]
    },
    {
        title: '地区榜',
        lists: [
            {name:'华语榜', value: {tag: 2}},
            {name:'日语榜', value: {tag: 3}},
            {name:'韩语榜', value: {tag: 4}},
            {name:'欧美榜', value: {tag: 5}}
        ]
    },
    {
        title: '特色榜',
        lists: [
            {name:'游戏音乐榜', value: {tag: 24}},
            {name:'动漫音乐榜', value: {tag: 22}}
        ]
    },
]