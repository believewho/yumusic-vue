export const singerType = [
  {
      title: '地区',
      entityType: '10',
      icon: 'icon-diqu',
      lists: [
          { value: '中国', type: 100 },
          { value: '欧美', type: 101 },
          { value: '日本', type: 102 },
          { value: '韩国', type: 103 }
      ]
  },
  {
      title: '性别',
      entityType: '12',
      icon: 'icon-xingbie',
      lists: [
          { value: '男', type: 300 },
          { value: '女', type: 301 },
          { value: '组合', type: 302 },
      ]
  },
  {
      title: '风格',
      entityType: '13',
      icon: 'icon-fengge',
      lists: [
          { value: '流行', type: 400 },
          { value: '摇滚', type: 401 },
          { value: '电子', type: 402 },
          { value: '古风', type: 403 },
          { value: '说唱', type: 404 },
          { value: '轻音乐', type: 405 },
      ]
  },
]
