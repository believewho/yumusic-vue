import {HttpManager} from '@/api';



export function getConst() {
    HttpManager.getTags(0).then(data => {
        if (data.code === 0) {
            var styleList = []
            for (var i = 0; i < data.data.length; i++) {
                var styleItem = {
                    title: data.data[i].name,
                    entityType: data.data[i].type,
                    icon: 'icon-yuzhong',
                    lists: data.data[i].children
                }
                styleList.push(styleItem)
            }
            return styleList
        }
    })
}
export const styleList = [
    {
        title: '语种',
        entityType: '10',
        icon: 'icon-yuzhong',
        lists: [
            { value: '华语', type: 100 },
            { value: '欧美', type: 101 },
            { value: '日语', type: 102 },
            { value: '韩语', type: 103 }
        ]
    },
    {
        title: '情感',
        entityType: '11',
        icon: 'icon-qinggan',
        lists: [
            { value: '怀旧', type: 200 },
            { value: '清新', type: 201 },
            { value: '浪漫', type: 202 },
            { value: '伤感', type: 203 },
            { value: '放松', tyoe: 204 },
            { value: '快乐', tyoe: 205 },
            { value: '安静', tyoe: 206 },
        ]
    },
    {
        title: '主题',
        entityType: '12',
        icon: 'icon-zhuti',
        lists: [
            { value: '影视原声', type: 300 },
            { value: 'ACG', type: 301 },
            { value: '校园', type: 302 },
            { value: '游戏', type: 303 },
            { value: 'KTV', type: 304 },
            { value: '榜单', type: 305 },
        ]
    },
    {
        title: '风格',
        entityType: '13',
        icon: 'icon-fengge',
        lists: [
            { value: '流行', type: 400 },
            { value: '摇滚', type: 401 },
            { value: '电子', type: 402 },
            { value: '古风', type: 403 },
            { value: '说唱', type: 404 },
            { value: '轻音乐', type: 405 },
        ]
    },
]
