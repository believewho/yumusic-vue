export const EntityType = {
    COLLECT_ALBUM : 0,
    COLLECT_PLAYLIST: 1,
    COLLECT_SONG: 2,
    FOCUS_USER: 3,
    FOCUS_ARTIST: 4,
}
export const SearchEntityType = {
    SONG: 1,
    ALBUM: 2,
    PLAYLIST: 3,
    ARTIST: 4
}

export const CommentConst = {
    COMMENT_SONG: 1,
    COMMENT_ARTIST: 2,
    COMMENT_PLAYLIST: 3,
    COMMENT_ALBUM: 4,
    COMMENT_COMMENT: 5,
}