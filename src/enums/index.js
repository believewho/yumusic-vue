import {Singer} from './singer'
import {songList} from './songList'
import {tagChange} from './album'
export {
    Singer,
    songList,
    tagChange
}