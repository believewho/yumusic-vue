import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'
import Home from '../views/Home.vue'
import FindMusic from '../components/FindMusic.vue'
import Recommend from '../views/Recommend.vue'
import PlayList from '../views/find/PlayList.vue'
import Artist from '../views/find/Artist.vue'
import Search from '../views/Search.vue'

import ResAlbum from '../views/searchResult/Album.vue'
import ResPlayList from '../views/searchResult/PlayList.vue'
import ResSingle from '../views/searchResult/Single.vue'


Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    name: '登录',
    component: Login
  },
  {
    path: '/',
    name: '主页',
    component: Home,
    redirect: '/find/recommend',
    children: [
      {
        path: '/find',
        component: FindMusic,
        redirect: '/find/recommend',
        children: [
          {
            path: 'recommend',
            component: Recommend,
            meta: {
              title: 'yumusic ╮(￣▽ ￣)╭'
            },
          },
          {
            path: 'playlist',
            component: PlayList
          },
          {
            path: 'artist',
            component: Artist
          }
        ]
      },
      {
        path: '/search',
        component: Search,
        meta: { title: '发现新大陆 ≧◇≦' },
        children: [
          { path: 'single', component: ResSingle, meta: { title: '发现新大陆 ≧◇≦' } },
          { path: 'playlist', component: ResPlayList, meta: { title: '发现新大陆 ≧◇≦' } },
          { path: 'album', component: ResAlbum, meta: { title: '发现新大陆 ≧◇≦' } }
        ]
      },
      {
        path: '/myspace',
        name: '个人空间',
        component: () => import('../views/myspace/MyMusic.vue'),
        redirect: '/myspace/like/song',
        meta: { title: '欢迎回家~' },
        children: [
          {
            path: 'like',
            redirect: 'like/song',
            component: () => import('../views/myspace/like/Like.vue'),
            children: [
              {
                path: 'song',
                component: () => import('../views/myspace/like/Song.vue')
              },
              {
                path: 'playlist',
                component: () => import('../views/myspace/like/Playlist.vue')
              },
              {
                path: 'album',
                component: () => import('../views/myspace/like/Album.vue')
              },
            ]
          },
          {
            path: 'playlist',
            component: () => import('../views/myspace/playlist/Playlist.vue')
          },
          {
            path: 'focus',
            redirect: 'focus/artist',
            component: () => import('../views/myspace/focus/Focus.vue'),
            children: [
              {
                path: 'artist',
                component: () => import('../views/myspace/focus/Artist.vue')
              },
              {
                path: 'users',
                component: () => import('../views/myspace/focus/Users.vue')
              },
            ]
          },
          {
            path: 'fans',
            component: () => import('../views/myspace/fans/Fans.vue')
          },
          {
            path: 'song',
            component: () => import('../views/myspace/songManager/SongManager.vue')
          },
          {
            path: 'daily',
            component: () => import('../views/myspace/daily/Daily.vue')
          },
          {
            path: 'history',
            component: () => import('../views/myspace/hitstory/History.vue')
          }
        ]
      },
      {
        path: '/musicplayer',
        component: () => import('../views/musicplayer/MusicPlayer.vue'),
        redirect: '/musicplayer/playlist',
        meta: { title: '你来听歌啦 (๑´ㅂ`๑)' },
        children: [
          {
            path: 'playlist',
            component: () => import('../views/musicplayer/Playlist.vue'),
            meta: { keepAlive: true, title: '你来听歌啦 (๑´ㅂ`๑)' }
          },
          {
            path: 'userlist',
            component: () => import('../views/musicplayer/Userlist.vue'),
            meta: { keepAlive: true, title: '你来听歌啦 (๑´ㅂ`๑)' }
          },
          {
            path: 'historylist',
            component: () => import('../views/musicplayer/Historylist.vue'),
            meta: { keepAlive: true, title: '你来听歌啦 (๑´ㅂ`๑)' }
          }
        ]
      },
      {
        path: '/details/song',
        component: () => import('../views/details/Song.vue')
      },
      {
        path: '/details/artist',
        component: () => import('../views/details/Artist.vue'),
      },
      {
        path: '/details/album',
        component: () => import('../views/details/Album.vue')
      },
      {
        path: 'toplist',
        component: () => import('../views/Toplist/TopList.vue'),

      },
      {
        path: '/message',
        component: () => import("../views/Message.vue")
      },
    ]
  },
]

const router = new VueRouter({
  routes
})


router.beforeEach((to, from, next) => {
  document.title = to.meta.title ? to.meta.title : 'yumusic'
  next()
})
export default router
