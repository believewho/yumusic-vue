import { get, post } from './request'
// axios.defaults.baseURL = 'http://81.68.212.59:3000'

const HttpManager = {
  // 获取验证码
  getCaptcha: () => get('/auth/captcha'),
  // 注册
  register: (params) => post('/auth/register', params),
  login: (params) => post('/auth/login', params),
  // 获取歌词
  getLyric: (params) => get(`/music/lyrics/get/${params}`),

  
  // 获取标签
  getTags: (type) => get(`/music/tags/list-tree/${type}`),
  // 获取歌曲详情
  getSongDetails: (id) => get(`/music/song/details/${id}`),  
  // 获取歌单详情
  getAlbumDetails: (id, uid) => get(`/music/album/details/${id}/${uid}`),
  // 通过id获得相应的歌曲列表
  getSongList: (params) => post(`/music/song/post/songlist`, params),
  // 获取歌手详情信息
  getArtistDetails: (params) => get(`/music/artist/details/${params}`),

  // 获得专辑列表
  getAlbumList: (params) => get('/music/album/list', params),
  // 获得歌曲列表
  getSongListByParams: (params) => get("/music/song/list", params),
  
  
  /* 与用户相关的api */
  // 获取用户信息
  getUserInfo: (uid) => get(`/member/user/info/${uid}`),
  // 获得我喜欢相关的列表
  getLikeList: (params) => get('/member/user/like/list', params),
  // 创建歌单
  createPlaylist: (params) => post('/member/user/create/playlist', params),
  // 获取用户创建的歌单
  getUserPlaylist: (uid) => get(`/member/user/playlist/${uid}`),
  // 上传头像
  uploadAvatar: (file) => post('/member/user/upload/avatar', file),
  // 上传歌单封面
  uploadCover: (file) => post('/member/user/upload/cover', file),
  // 上传歌曲
  uploadSong: (params) => post('/member/user/upload/song', params),
  // 获取用户上传的歌曲列表
  getMySongs: (params, uid) => get(`/member/user/my/songs/${uid}`, params),
  // 收藏
  collect: (params) => get('/member/user/collect', params),
  // 取消收藏
  uncollect: (params) => get('/member/user/uncollect', params),
  // 关注
  follow: (params) => get('/member/user/follow', params),
  // 取消关注
  unfollow: (params) => get('/member/user/unfollow', params),
  // 判断用户是否有该类型(收藏、点赞、关注等)
  iscollect: (params) => get('/member/user/iscontain/', params),
  // 添加评论
  addComment: (params) => post("/member/comment/add", params), 
  // 获取评论
  getComment: (params) => get('/member/comment/list', params),
  // 点赞
  like: (params) => post('/member/like/add', params),
  // 用户上传歌曲
  uploadSongFile: (params) => post('/music/song/upload', params),
  // 获得用户的粉丝列表
  getFansList: (params) => get('/member/user/fans', params),
  // 获得用户的回复消息
  getReplyList: (params) => get('/member/message/list', params),
  // 获取未读消息的数量
  getMessageCount: () => get("/member/message/count"),
  /* 搜索 */
  search: (params) => get('/recommend/search/list', params),
  // 热搜
  getHotSearch: (params) => get('/recommend/hot/top', params),
  // 歌曲排行
  getHotlist: (params) => get('/music/song/hotlist', params),
  // 热门专辑
  getHotAlbum: (params) => get('/music/album/hot/album', params),
  // 获得歌手列表
  getArtistList: (params) => get('/music/artist/list', params),
  // 获得用户的每日推荐
  getDailySong: (params) => get('/member/recommend/daily', params),
  // 用户添加歌曲到自己创建的歌单
  tracksPlaylist: (params) => get('/member/user/playlist/tracks', params),
  // 用户播放量+1
  playcountIncrease: (params) => get('/member/history/increase', params),
  // 查询用户的历史播放列表
  userHistory: (params) => get('/member/history/list', params)
}


export {HttpManager}